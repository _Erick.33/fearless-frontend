function createCard(name, description, pictureUrl, starts, ends, locationName) {
  return `
    <div class="card mb-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-secondary">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
     </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {

    } else {
      const data = await response.json();
      let index = 0

      for (let conference of data.conferences) {
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title')
        // nameTag.innerHTML = conference.name

        const detailUrl = `http://localhost:8000${conference.href}`
        const detailResponse = await fetch(detailUrl)
        if (detailResponse.ok) {
            const details = await detailResponse.json()
            console.log(details)
            const title = details.conference.name
            const description = details.conference.description
            // description.innerHTML = details.conference.description
            const pictureUrl = details.conference.location.picture_url
            const starts = details.conference.starts
            const ends = details.conference.ends
            const locationName = details.conference.location.name
            // image.src = details.conference.location.picture_url
            //console.log(locationName)
            const html = createCard(title, description, pictureUrl, starts, ends, locationName)
            const column = document.querySelector(`#col-${index}`)
            column.innerHTML += html
            index += 1
            if (index > 2) {
                index = 0
            }

        }
      }

    }
  } catch (e) {

  }

});
