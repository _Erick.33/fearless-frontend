import React, { useEffect, useState } from 'react'


function ConferenceForm(props) {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [desc, setDesc] = useState('')
    const [maxPres, setMaxPres] = useState('')
    const [maxAtt, setMaxAtt] = useState('')
    const [selectedLocation, setSelectedLocation] = useState('')

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)

        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleStartChange = (event) => {
        const value = event.target.value
        setStart(value)
    }

    const handleEndChange = (event) => {
        const value = event.target.value
        setEnd(value)
    }

    const handleDescChange = (event) => {
        const value = event.target.value
        setDesc(value)
    }

    const handleMaxPresChange = (event) => {
        const value = event.target.value
        setMaxPres(value)
    }

    const handleMaxAttChange = (event) => {
        const value = event.target.value
        setMaxAtt(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setSelectedLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name
        data.starts = start
        data.ends = end
        data.description = desc
        data.max_presentations = maxPres
        data.max_attendees = maxAtt
        data.location = selectedLocation
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)

            setName('')
            setStart('')
            setEnd('')
            setDesc('')
            setMaxPres('')
            setMaxAtt('')
            setSelectedLocation('')
        }

    }

    useEffect(() => {
        fetchData()

    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStartChange} value={start} placeholder="Starts" required type="date" name="starts" id="starts"
                                    className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEndChange} value={end} placeholder="Ends" required type="date" name="ends" id="ends"
                                    className="form-control" />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description">Description</label>
                                <textarea onChange={handleDescChange} value={desc} className="form-control" id="description" rows="3" name="description"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxPresChange} value={maxPres} placeholder="Maximum presentations" required type="number"
                                    name="max_presentations" id="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">Maximum presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxAttChange} value={maxAtt} placeholder="Maximum attendees" required type="number" name="max_attendees"
                                    id="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">Maximum attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(locations => {
                                        return (
                                            <option key={locations.id} value={locations.id}>
                                                {locations.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ConferenceForm
